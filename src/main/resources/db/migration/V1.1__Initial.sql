/*! SET storage_engine=INNODB */;

DROP TABLE IF EXISTS users;
CREATE TABLE users (
  user_id SERIAL,
  email varchar(128) NOT NULL,
  first_name varchar(128) NOT NULL,
  last_name varchar(128) NOT NULL,
  password varchar(128) NOT NULL,
  secret_key varchar(512) DEFAULT NULL,
  created_date date NOT NULL,
  modified_date date DEFAULT NULL,
  PRIMARY KEY (user_id)
);
