package com.nickinem.auth.controller;

import com.nickinem.auth.entity.User;
import com.nickinem.auth.service.SecurityService;
import com.nickinem.auth.service.UserService;
import com.nickinem.auth.wire.SignUpUserDto;
import com.nickinem.auth.wire.UserDto;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Controller
public class AuthController {

    private final UserService userService;
    private final SecurityService securityService;
    private final ModelMapper modelMapper;

    @PostMapping("/signup")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public UserDto signUp(@RequestBody final SignUpUserDto signUpDto) {
        log.info("Attempt to sign up user with email: {}", signUpDto.email);

        final User user = userService.signUp(modelMapper.map(signUpDto, User.class));
        securityService.login(user.getEmail(), signUpDto.password);
        /*There should also be roles*/
        return modelMapper.map(user, UserDto.class);
    }

}
