package com.nickinem.auth.controller;

import com.nickinem.auth.configuration.jwt.JwtTokenProvider;
import com.nickinem.auth.service.SecurityService;
import com.nickinem.auth.util.AuthHeaderType;
import com.nickinem.auth.util.TokenUtil;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Controller
public class JwtAuthController {

    private final SecurityService securityService;

    private final JwtTokenProvider jwtTokenProvider;

    @PostMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestHeader(HttpHeaders.AUTHORIZATION) final String authentication) {
        final Pair<String, String> usernameToPassword =
                TokenUtil.parseBaseEncodedHeader(AuthHeaderType.BASIC, authentication);

        securityService.login(usernameToPassword.getLeft(), usernameToPassword.getRight());

        final String token = jwtTokenProvider.generateToken(usernameToPassword.getLeft());
        return ResponseEntity.ok()
                             .header(HttpHeaders.AUTHORIZATION, TokenUtil.prepareToken(AuthHeaderType.BEARER, token))
                             .build();
    }

}