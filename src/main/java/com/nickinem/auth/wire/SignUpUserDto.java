package com.nickinem.auth.wire;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Mykola Morozov
 */
public class SignUpUserDto extends LoginUserDto {

    @JsonProperty("first_name")
    public String firstName;

    @JsonProperty("last_name")
    public String lastName;

}
