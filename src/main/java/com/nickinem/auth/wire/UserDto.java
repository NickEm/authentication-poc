package com.nickinem.auth.wire;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author Mykola Morozov
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto extends SignUpUserDto {

    @JsonProperty("user_id")
    public Long userId;

    @JsonProperty("secret_key")
    public String secretKey;

    @JsonProperty("roles")
    public List<Long> roles;

}
