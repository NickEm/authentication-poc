package com.nickinem.auth.wire;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginUserDto {

    @JsonProperty("email")
    public String email;

    @JsonProperty("password")
    public String password;
}
