package com.nickinem.auth.wire;

import lombok.Value;

@Value
public class JwtResponse {

    private final String jwtToken;
}
