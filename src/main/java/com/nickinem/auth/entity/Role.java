package com.nickinem.auth.entity;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Mykola Morozov
 */

@Data
@Entity
@Table(name = "ROLE")
public class Role {

    @Id
    @Access(AccessType.PROPERTY)
    @GeneratedValue
    @Column(name = "ROLE_ID")
    private Long roleId;

    @Column(name = "ROLE_TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private RoleType roleType;

}
