package com.nickinem.auth.entity;

/**
 * @author Mykola Morozov
 */
public enum RoleType {

    POWERFUL,
    USER;
}
