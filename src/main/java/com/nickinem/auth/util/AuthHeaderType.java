package com.nickinem.auth.util;

public enum  AuthHeaderType {
    BASIC("Basic "),
    BEARER("Bearer ");

    private final String prefix;

    AuthHeaderType(final String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }
}
