package com.nickinem.auth.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import lombok.experimental.UtilityClass;

@UtilityClass
public class TokenUtil {

    private static final String HEADER_VALUE_FORMAT = "%s %s";
    private static final String CREDENTIALS_SEPARATOR = ":";

    public static String prepareToken(final AuthHeaderType headerType, final String value) {
        return String.format(HEADER_VALUE_FORMAT, headerType.getPrefix(), value);
    }

    public static Pair<String, String> parseBaseEncodedHeader(final AuthHeaderType headerType, final String header) {
        if (header != null && header.startsWith(headerType.getPrefix())) {
            final byte[] credDecoded = Base64.getDecoder().decode(header.substring(headerType.getPrefix().length()).trim());
            final String credentials = new String(credDecoded, StandardCharsets.UTF_8);
            final String[] values = credentials.split(CREDENTIALS_SEPARATOR, 2);

            return Pair.of(values[0], values[1]);
        }

        return Pair.of(StringUtils.EMPTY, StringUtils.EMPTY);
    }

    public static String parseRawHeader(final AuthHeaderType headerType, final String header) {
        if (header != null && header.startsWith(headerType.getPrefix())) {
            return header.substring(headerType.getPrefix().length()).trim();
        }

        return null;
    }

}
