package com.nickinem.auth.repository;

import com.nickinem.auth.entity.Role;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
