package com.nickinem.auth.service;

import com.nickinem.auth.entity.RoleType;
import com.nickinem.auth.entity.User;
import com.nickinem.auth.repository.UserRepository;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(final String username) {
        final User user = userRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException(username));

        /*TODO: Update roles*/
        final Set<GrantedAuthority> grantedAuthorities = Arrays.stream(RoleType.values())
                                                               .map(role -> new SimpleGrantedAuthority(role.name()))
                                                               .collect(Collectors.toSet());

        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), grantedAuthorities);
    }
}
