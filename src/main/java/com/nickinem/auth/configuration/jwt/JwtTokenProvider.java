package com.nickinem.auth.configuration.jwt;

import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class JwtTokenProvider {

    private final JwtProperties jwtProperties;

    public String getUsernameFromToken(final String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public Date getExpirationDateFromToken(final String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(final String token, final Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    /*for retrieving any information from token we will need the secret key*/
    private Claims getAllClaimsFromToken(final String token) {
        return Jwts.parser().setSigningKey(jwtProperties.getSecret()).parseClaimsJws(token).getBody();
    }

    public Boolean validateToken(final String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.after(Date.from(Instant.now()));
    }

    public String generateToken(final String username) {
        final Map<String, Object> claims = new HashMap<>();
        return generateToken(claims, username);
    }

    //1. Define  claims of the token, like Issuer, Expiration, Subject, and the ID
    //2. Sign the JWT using the HS512 algorithm and secret key.
    //3. According to JWS Compact Serialization compaction of the JWT to a URL-safe string
    private String generateToken(final Map<String, Object> claims, final String subject) {
        final Date current = Date.from(Instant.now());
        final Date expiration = new Date(current.getTime() + jwtProperties.getExpirationInterval() * 1000);

        return Jwts.builder()
                   .setClaims(claims)
                   .setSubject(subject)
                   .setIssuedAt(current)
                   .setExpiration(expiration)
                   .signWith(SignatureAlgorithm.HS512, jwtProperties.getSecret())
                   .compact();
    }
}
