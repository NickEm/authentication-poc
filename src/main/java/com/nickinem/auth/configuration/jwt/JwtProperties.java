package com.nickinem.auth.configuration.jwt;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "auth-app.jwt")
public class JwtProperties {

    private String secret;
    private long expirationInterval;
}
