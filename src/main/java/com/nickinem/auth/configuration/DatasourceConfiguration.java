package com.nickinem.auth.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import lombok.RequiredArgsConstructor;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.nickinem.auth.repository")
@RequiredArgsConstructor
public class DatasourceConfiguration {

    private final DatasourceProperties properties;

    @ConditionalOnProperty(name = "auth-app.datasource.enabled", havingValue = "true")
    @Bean(name = "dataSource")
    public HikariDataSource dataSourceMigration() {
        final HikariConfig config = new HikariConfig();
        config.setDriverClassName(properties.getDriverClassName());
        config.setJdbcUrl(properties.getJdbcUrl());
        config.setUsername(properties.getUsername());
        config.setPassword(properties.getPassword());

        config.setPoolName(properties.getPoolName());
        config.setMaximumPoolSize(properties.getMaximumPoolSize());
        config.setMinimumIdle(properties.getMinimumIdle());
        config.setConnectionTimeout(properties.getConnectionTimeout());
        config.setSchema(properties.getName());
        config.addDataSourceProperty("databaseName", properties.getName());

        return new HikariDataSource(config);
    }

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean sessionFactory(final EntityManagerFactoryBuilder builder,
                                                                 @Qualifier("dataSource") final DataSource dataSource) {
        final Map<String, String> emfProperties = new HashMap<>();
        emfProperties.put("hibernate.dialect", properties.getDialect());
        emfProperties.put("hibernate.connection.release_mode", "after_transaction");
        final LocalContainerEntityManagerFactoryBean emf = builder.dataSource(dataSource)
                                                            /*TODO: Don't forget to update*/
                                                            .packages("com.nickinem.auth.entity")
                                                            .properties(emfProperties)
                                                            .build();

        emf.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

        return emf;
    }

    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager(
            @Qualifier("entityManagerFactory") final EntityManagerFactory entityManagerFactory,
            @Qualifier("dataSource") final DataSource dataSource) {
        final JpaTransactionManager tm = new JpaTransactionManager(entityManagerFactory);
        tm.setDataSource(dataSource);
        return tm;
    }
}
