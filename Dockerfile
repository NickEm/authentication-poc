FROM azul/zulu-openjdk:11.0.5
VOLUME /tmp
COPY build/libs/authentication-poc-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]