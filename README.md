## Authentication POC

### Intention

Intention is to demonstrate some auth patterns, common mistakes and vulnerabilities

### Prerequisites

* You should have `java-11` installed 
* You should have `docker` and `docker-compose` installed

### Installation

1. You can start application
 
		./gradlew clean build && docker-compose -f docker-compose-app.yml up --build

2. To signup new user use `signup` endpoint. F.e: 
        
        curl -X POST \
          http://localhost:8082/signup \
          -H 'Content-Type: application/json' \
          -d '{
          	"first_name": "yourName",
          	"last_name": "yourSurname",
            "email": "yourmail@gmail.com",
            "password": "yourPassword"
        }'
        
2. To generate new JWT token in responce header use:

		curl -X POST \
          http://localhost:8082/authenticate \
          -H 'Authorization: Basic eW91cm1haWxAZ21haWwuY29tOnlvdXJQYXNzd29yZA=='
		
3. You should be able to trigger another auth protected endpoints with provided authentication header:

		curl -X GET \
          http://localhost:8082/users \
          -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJuaWNraW5lbTIzQGdtYWlsLmNvbSIsImV4cCI6MTU3NTQxMTM1MCwiaWF0IjoxNTc1MzkzMzUwfQ.epjVcowPRtqVTeRsEAda9YQkVplXFkNkk5nhyhebOe_XO3rOQz9FQc9xhYTdT5BVynkkhDzEFMsE4L2c-o2hgg'		 



### Useful links

[JWT Explained video](http://youtube.com/watch?v=7Q17ubqLfaM)